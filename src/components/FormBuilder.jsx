import React from 'react';
import { useDynamicFormBuilder } from '../hooks/useDynamicFormBuilder';

export default function FormBuilder(props) {
  const fields = props.formJSON[0].fields;
  const { data, validate, buildFormComponents } = useDynamicFormBuilder(fields);

  const handleSubmit = (e) => {
    e.preventDefault();

    validate(data, (errors) => {
      if (errors === null || Object.keys(errors).length === 0) {
        const mapData = Array.from(data).map(([id, obj]) => ({ [id]: obj.value }));
        console.log(mapData);
      }
    });
  };
  return (
    <form className="container w-25 mt-5" onSubmit={handleSubmit}>
      {buildFormComponents()}
      <div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </div>
    </form>
  );
}
