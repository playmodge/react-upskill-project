import './Label.scss';

export default function Label({ id, label, styleName = '' }) {
  return (
    <label className={`${styleName}`} htmlFor={id}>{label}</label>
  );
}
