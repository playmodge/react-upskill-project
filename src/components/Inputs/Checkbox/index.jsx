import Label from '../../Label';

export default function Checkbox(props) {
  const {
    onChange,
    errors,
    data: { value, id, ref, label, type, name, required }
  } = props;
  return (
    <div className="form-check my-1 mx-3">
      <Label id={id} label={label} styleName={required && 'asterisk'} />
      <input
        className="form-check-input"
        type={type}
        id={id}
        data-ref={ref}
        name={name}
        value={value}
        checked={value}
        onChange={onChange}
      ></input>
      {errors && <div className="field-error">{errors[id]}</div>}
    </div>
  );
}
