export const formJSON = [
  {
    fields: [
      {
        id: 'firstname',
        label: 'First Name',
        required: true,
        placeholder: 'Enter your first name ...',
        type: 'text',
        value: ''
      },
      {
        id: 'lastname',
        label: 'Last Name',
        required: false,
        placeholder: 'Enter your last name ...',
        type: 'text',
        value: ''
      },
      {
        id: 'country',
        label: 'County',
        required: true,
        type: 'select',
        options: [
          { label: 'Bulgaria' },
          { label: 'Greece' },
          { label: 'Romania' },
          { label: 'Serbia' }
        ],
        value: ''
      },
      {
        id: 'city',
        label: 'Choose a city',
        required: true,
        type: 'radio',
        options: [
          { label: 'Pleven' },
          { label: 'Sofia' },
          { label: 'Perushtica' },
          { label: 'Vratsa' }
        ],
        value: '',
        ref: 'subscribe to city'
      },
      {
        id: 'email',
        label: 'E-mail',
        required: true,
        type: 'text',
        value: '',
        ref: 'subscribe'
      },
      {
        id: 'library',
        label: 'Library',
        required: true,
        type: 'textarea',
        value: '',
        ref: 'subscribe to library'
      },
      {
        id: 'subscribe',
        label: 'Subscribe to newsletter',
        required: true,
        type: 'checkbox',
        value: '',
        
      },
      {
        id: 'subscribe to library',
        label: 'Subscribe to Library',
        required: false,
        type: 'checkbox',
        value: '',
        
      },
      {
        id: 'subscribe to city',
        label: 'subscribe to City',
        required: false,
        type: 'checkbox',
        value: '',
      }
    ]
  }
];
