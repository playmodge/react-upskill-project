import Label from '../../Label';
import Error from '../Error';

export default function Textarea(props) {
  const {
    onChange,
    errors,
    data: { value, id, label, type, name, required }
  } = props;
  return (
    <div className="m-3">
      <Label id={id} label={label} styleName={required && 'asterisk'} />
      <textarea
        className="form-control"
        type={type}
        id={id}
        name={name}
        value={value}
        onChange={onChange}
      />
      <Error errors={errors} id={id} />
    </div>
  );
}
