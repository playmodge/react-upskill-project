import Text from '../Inputs/Text';
import Select from '../Inputs/Select';
import Checkbox from '../Inputs/Checkbox';
import Textarea from '../Inputs/Textarea';
import Radio from '../Inputs/Radio';

export default {
  Text,
  Select,
  Checkbox,
  Radio,
  Textarea
};
