import Label from '../../Label';
import Error from '../Error';

export default function Select(props) {
  const {
    onChange,
    errors,
    data: { options, id, label, type, name, required }
  } = props;

  const defaultOption = <option disabled hidden></option>;
  const createOption = (o, i) => (
    <option key={i} value={o.label}>
      {o.label}
    </option>
  );
  const mappedOptions = options.map(createOption);
  return (
    <div className="m-3">
      <Label id={id} label={label} styleName={required && 'asterisk'} />
      <select
        className="form-select"
        defaultValue=""
        type={type}
        id={id}
        name={name}
        onChange={onChange}
      >
        {defaultOption}
        {mappedOptions}
      </select>
      <Error errors={errors} id={id} />
    </div>
  );
}
