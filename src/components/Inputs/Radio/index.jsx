import Label from '../../Label';
import Error from '../Error';

export default function Radio(props) {
  const {
    onChange,
    errors,
    data: { options, label, type, id, required }
  } = props;

  const createOption = (o, i) => (
    <div className="form-check" key={i}>
      <Label id={o.label} label={o.label} />
      <input
        className="form-check-input"
        id={o.label}
        value={o.label}
        type={type}
        id={id}
      />
    </div>
  );
  const radioOpts = options.map(createOption);
  return (
    <div className="m-3">
      <Label id={label} label={label} styleName={required && 'asterisk'} />
      <div className="mb-2" onChange={onChange}>
        {radioOpts}
      </div>
      <Error errors={errors} id={id} />
    </div>
  );
}
