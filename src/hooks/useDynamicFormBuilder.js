import React from 'react';
import InputBuilder from '../components/InputBuilder';
import { useErrorValidation } from './useErrorValidation';

export const useDynamicFormBuilder = (fields) => {
  const [data, setData] = React.useState(new Map());
  const { errors, validate, clearErrors } = useErrorValidation();

  React.useEffect(() => {
    const newData = new Map();

    fields.forEach((field) => newData.set(field.id, field));
    handleShowProp(newData);
    setData(newData);
  }, []);

  const handleShowProp = (data) => {
    Array.from(data).forEach(([id, obj]) => {
      if (obj.ref) {
        const checkBoxRefValue = data.get(obj.ref).value;
        if (checkBoxRefValue) {
          obj.show = true;
        } else {
          obj.value = '';
          obj.show = false;
        }
      } else {
        obj.show = true;
      }
    });
  };

  const handleInput = (e) => {
    const el = e.target;

    const newData = new Map(data);
    const fields = newData.get(el.id);
    el.type === 'checkbox' ? (fields.value = el.checked) : (fields.value = el.value);

    if (fields.show && fields.required && !fields.value) {
      validate({ [fields.id]: `${fields.id} is required!` });
    } else {
      clearErrors(fields.id);
    }
    handleShowProp(newData);
    setData(newData);
  };

  const createInputComponent = (data) => {
    return (
      <InputBuilder
        key={data.id}
        data={data}
        errors={errors}
        onChange={handleInput}
      />
    );
  };

  const buildFormComponents = () => {
    const newData = Array.from(data);
    return newData.map(([id, obj]) => {
      return obj.show ? createInputComponent(obj) : null;
    });
  };

  return { data, validate, buildFormComponents };
};
