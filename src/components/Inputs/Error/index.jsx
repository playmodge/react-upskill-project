import './Error.scss';

export default function Error({ errors, id }) {
  return <>{errors && <div className="field-error">{errors[id]}</div>}</>;
}
