import React from 'react';

export const useErrorValidation = () => {
  const [errors, setErrors] = React.useState(null);

  const validate = (obj, cb) => {
    if (Object.keys(obj).length) {
      setErrors((prevErrors) => {
        cb && cb({ ...prevErrors, ...obj });
        return { ...prevErrors, ...obj };
      });
    } else {
      const arrayFromMap = Array.from(new Map(obj));

      let hasNewErrors = false;
      arrayFromMap.forEach(([id, obj]) => {
        if (obj.show && obj.required && !obj.value) {
          hasNewErrors = true;
          setErrors((prevErrors) => {
            cb && cb({ ...prevErrors, [id]: `${obj.id} is required!` });
            return {
              ...prevErrors,
              [id]: `${obj.id} is required!`
            };
          });
        }
      });
      if (!hasNewErrors) {
        cb && cb({});
        hasNewErrors = false;
      }
    }
  };

  const clearErrors = (key) => {
    const updateErrors = { ...errors };
    delete updateErrors[key];
    setErrors(updateErrors);
  };

  return { errors, validate, clearErrors };
};
