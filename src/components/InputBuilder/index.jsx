import Inputs from '../Inputs';

export default function InputBuilder(props) {
  const { data } = props;

  const type = data.type.charAt(0).toUpperCase() + data.type.slice(1);
  const Input = Inputs[type];

  return <Input {...props} />;
}
