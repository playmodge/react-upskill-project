import './App.scss';
import 'bootstrap/dist/css/bootstrap.css';
import { formJSON } from './data';
import FormBuilder from './components/FormBuilder';

export const App = () => <FormBuilder formJSON={formJSON} />;